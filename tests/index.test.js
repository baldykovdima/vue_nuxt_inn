import { shallowMount } from "@vue/test-utils";
import IndexPage from "~/pages/index.vue";

test("displays index page", () => {
  const wrapper = shallowMount(IndexPage);
  expect(wrapper.find("h2").text()).toBe("Введите ИНН для проверки");
});
