import { shallowMount } from "@vue/test-utils";
import List from "@/components/List.vue";

test("displays index page", () => {
  const wrapper = shallowMount(List);
  expect(wrapper.isVueInstance()).toBeTruthy();
});
