import { shallowMount } from "@vue/test-utils";
import Form from "@/components/Form.vue";

test("displays index page", () => {
  const wrapper = shallowMount(Form);
  expect(wrapper.isVueInstance()).toBeTruthy();
});
